package com.mentoring.hazelcastcache;

import com.mentoring.services.PersonService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HazelcastCacheApplicationTests {

    @Autowired
    private PersonService personService;
    
    @Test
    public void contextLoads() {}
        
    @Test
    public void personTest() {
        Temporal init = LocalDateTime.now();
        personService.byName("Gonzalo").ifPresent(System.out::println);
        System.out.println("Duration 1: " + this.dateTimeDifference(init, LocalDateTime.now(), ChronoUnit.MILLIS));
        
        init = LocalDateTime.now();
        personService.byName("Gonzalo").ifPresent(System.out::println);
        System.out.println("Duration 2: " + this.dateTimeDifference(init, LocalDateTime.now(), ChronoUnit.MILLIS));
    }

    private long dateTimeDifference(Temporal d1, Temporal d2, ChronoUnit unit){
        return unit.between(d1, d2);
    }
}
