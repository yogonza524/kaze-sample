/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mentoring.hazelcastcache;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author gonzalo
 */
@Configuration
public class HazelCastConfig {
    
    @Bean
        Config haszelcastConfig() {
            return new Config().setInstanceName("haz-config-instance")
                    .addMapConfig(
                        new MapConfig()
                            .setName("persons")
                            .setEvictionPolicy(EvictionPolicy.LRU)
                            .setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
                            .setTimeToLiveSeconds(3600)
                    );                   
        }
}
