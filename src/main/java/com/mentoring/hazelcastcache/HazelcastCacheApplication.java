package com.mentoring.hazelcastcache;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.mentoring.services.PersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

@EnableCaching
@SpringBootApplication
public class HazelcastCacheApplication {

	public static void main(String[] args) {
            SpringApplication.run(HazelcastCacheApplication.class, args);
	}
        
        @Bean
        PersonService personService() {
            return new PersonService();
        }
}
