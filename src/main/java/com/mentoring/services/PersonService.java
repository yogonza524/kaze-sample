/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mentoring.services;

import com.mentoring.model.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author gonzalo
 */
@Service
@CacheConfig(cacheNames = "persons")
public class PersonService {
    
    private List<Person> persons;
    
    @PostConstruct
    private void init() {
        this.persons = new ArrayList<>();
        this.persons.add(new Person("Gonzalo", "Mendoza"));
        this.persons.add(new Person("Rodrigo", "Mendoza"));
        this.persons.add(new Person("Fabio", "Mendoza"));
        this.persons.add(new Person("Sofia", "Mendoza"));
        this.persons.add(new Person("Julio", "Mendoza"));
    }
    
    @CacheEvict(allEntries = true)
    public void clearCache() {}
    
    @Cacheable(condition = "#name.equals('Gonzalo')")
    public Optional<Person> byName(String name) {
        System.out.println("Buscando a la persona con nombre: " + name);
        return this.persons.stream().filter(p -> p.getName().equals(name)).findAny();
    }
}
